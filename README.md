# Isis Flag Status - KaiOS App

## Description

This KaiOS app displays the flag status for the Isis River, as set by OURCs (Oxford University Rowing Clubs). It provides information about the river's flag status, additional rules that may apply, and details on when and by whom the status was set.

![App Screenshot 1](./screenshots/screenshot-1.png)
![App Screenshot 2](./screenshots/screenshot-2.png)
![App Screenshot 3](./screenshots/screenshot-3.png)

## Usage

To use the app, open it in the KaiOS WebIDE, and click the "Run" button to launch it in the KaiOS Runtime (kaiosrt). It has been tested on the Nokia 800 Tough, compatablitiy with any other devices is not known.

## Installation

1. Clone this repository to your local machine.
2. Open the app folder in the KaiOS WebIDE.
3. Click the "Run" button in the WebIDE to install and launch the app in KaiOS Runtime.

## How It Gets Data

The app retrieves flag status data from the OURCs API at [https://ourcs.co.uk/](https://ourcs.co.uk/). It periodically fetches the latest status and displays it in the app.

## License

This app is open-source and licensed under the [GNU General Public License, version 3 (GPL-3.0)](https://www.gnu.org/licenses/gpl-3.0.html).

## Collaboration

Collaboration and contributions to this project are welcome. Feel free to visit my website [here](https://users.ox.ac.uk/~ball5903/) for contact details and more information.

## Learning Opportunity

This app is a great resource for learning simple JavaScript and HTML programming. It provides a practical example of web development for the KaiOS platform. Based on some stuff from Copyright (c) 2019 Christian Waadt in KaiOS-Sample.

