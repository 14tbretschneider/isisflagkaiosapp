(function (app) {
    window.addEventListener("load", function () {
        var extrasContainer = document.getElementById('generatedExtra');
        populateExtras(extrasContainer);
    })

    function populateExtras(container) {
        container.innerHTML = '';
        const notices = data.notices;
    if (notices && notices.length > 0) {
        const noticesList = document.createElement('ul');
        for (let i = 0; i < notices.length; i++) {
            const noticeItem = document.createElement('li');
            const noticeText = document.createElement('p');
            noticeText.textContent = `Notice ${i + 1}: ${notices[i]}`;
            noticeItem.appendChild(noticeText);
            noticesList.appendChild(noticeItem);
        }
        container.appendChild(noticesList);
    }
          
    const setByDiv = document.createElement('div');
    const setByText = document.createElement('p');
    setByText.textContent = `Set by: ${data.set_by}, Date: ${data.set_date}`;
    setByDiv.appendChild(setByText);
    container.appendChild(setByDiv);
    }

    return app;
}(MODULE));