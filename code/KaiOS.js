(function (app) {
	//input mapping
	document.addEventListener('keydown', handleKeydown);
	document.addEventListener('keyup', handleKeyup);

	var keyOverlay = document.getElementById('keyOverlay');
	var overlayTimeout;

	function handleKeydown(e) {
		// enable key overlay
	//	keyOverlay.style.display = 'block';
	//	keyOverlay.innerHTML += '<span>' + e.key + '</span>';
		console.log('Button pressed:', e.key);

		// clear overlay
		clearTimeout(overlayTimeout);
		overlayTimeout = setTimeout(function () {
			keyOverlay.innerHTML = ' ';
			keyOverlay.style.display = 'none';
		}, 1000);

		switch (e.key) {
			case 'Backspace':
				e.preventDefault(); // prevent the app from closing
				break;
		}
	}

	function handleKeyup(e) {
		switch (e.key) {
			case 'ArrowUp':
			case '2': /* num pad navigation */
				app.keyCallback.dUp();
				break;
			case 'ArrowDown':
			case '8': /* num pad navigation */
				app.keyCallback.dDown();
				break;
			case 'ArrowLeft':
			case '4': /* num pad navigation */
				app.keyCallback.dLeft();
				break;
			case 'ArrowRight':
			case '6': /* num pad navigation */
				app.keyCallback.dRight();
				break;
			case 'SoftLeft':
			case 'Control': /* use on PC */
				app.keyCallback.softLeft();
				break;
			case 'SoftRight':
			case 'Alt': /* use on PC */
				app.keyCallback.softRight();
				break;
			case 'Enter':
			case '5':
				app.keyCallback.enter();
				break;
			case 'ContextMenu':
				app.keyCallback.menu();
				break;
			case 'Backspace':
				app.keyCallback.back();
				break;
			case 'EndCall':
				app.keyCallback.quit();
				break;
			default:
				app.keyCallback.other(e.key);
		}
	}


	return app;
}(MODULE));